#!/bin/bash

echo "----------------Setup Project Location----------------"
cd ~
mkdir NodeProjects/restaurant && cd NodeProjects/restaurant


echo "-----------------Cloning Deployment--------------------"
echo "\n\n"
git clone https://azax25547@bitbucket.org/azax25547/restaurant-infra.git


echo "-----------------Cloning Auth-------------------"
echo "\n\n"
git clone https://azax25547@bitbucket.org/azax25547/auth.git
cd auth
npm install

cd ../

echo "-----------------Cloning Client-------------------"
echo "\n\n"
git clone https://azax25547@bitbucket.org/azax25547/client.git
cd client
npm install

cd ../

echo "-----------------Cloning Orders-------------------"
echo "\n\n"
git clone https://azax25547@bitbucket.org/azax25547/orders.git
cd orders
npm install

cd ../

echo "-----------------Cloning Recipes-------------------"
echo "\n\n"
git clone https://azax25547@bitbucket.org/azax25547/recipes.git
cd recipes
npm install

cd ../