# Deployment Guide

## v1

V1 deployment is for Development...
For this we need to have all the mentioned services at one place...

1. First run `setup.sh` which will get all the services and then setup all the required files 
2. Next create a `rest-be-configmap.yaml` inside v1 and fill all the required below varaibles
    - MONGO_USERNAME:
    - MONGO_USERNAME_PASSWORD:
    - JWT_KEY:
    - SESSION_SECRET:
    - MONGO_DB_URI:

3. next run `skaffold dev`